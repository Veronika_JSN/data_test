var faker = require('faker');

exports.seed = async function(knex) {

  await knex('users').del();
  
  function getRandomInt(max) {
  return Math.floor(Math.random() * max);
  }

  let arr = [];

  for(let i=0; i<20; i++){
    let start_ip = getRandomInt(255);

    for(let j=0; j<10000; j++){  

      let ip = faker.internet.ip();
      let index = ip.indexOf('.')
      let newArr = ip.slice(index, ip.length)  

      arr.push({
        login: faker.internet.userName(),
        password: faker.unique(faker.internet.password), 
        registration_date: faker.date.between('2017-01-01', '2018-12-01'), 
        last_visit_date: faker.date.between('2018-01-01', '2018-12-01'),
        ip: start_ip + newArr,
        flag: faker.datatype.boolean()
      })
    }

  }
  knex.batchInsert('users', arr)
};