var faker = require('faker');

exports.seed = function(knex) {

  return knex('partners').del()
    .then(function () {

      let arr = [];
      for (let i=1; i<=10; i++){
          arr.push({partner_name: faker.unique(faker.name.findName)})
      }
      return knex('partners').insert(arr);
    });
};
