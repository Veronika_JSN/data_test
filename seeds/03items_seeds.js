var faker = require('faker');

exports.seed = function(knex) {

  return knex('items').del()
    .then(function () {

      return knex('items').insert([
          {item_name: faker.commerce.productName(), price: faker.commerce.price()},
          {item_name: faker.commerce.productName(), price: faker.commerce.price()},
      ]);
    });
};
