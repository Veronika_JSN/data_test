exports.seed = async function(knex) {

  await knex('users_group').del()

      function getRandomInt(max) {
          return Math.floor(Math.random() * max);
      }
      let uids = (await knex('users')).map(user => user.uid);

      let gids = await knex('groups');

      let admin = gids.find(i=>i.group_name == 'admin');
      
      let arrWithoutAdmin = gids.filter(i=>i.group_name !== 'admin');

      let arr = [];

      arr.push({user_id: uids[0], group_id: admin.id})

      for(let i=1; i<uids.length; i++){

        const countOfGroupe = getRandomInt(3) +1;

        let indexGr =[];
        
        for(let j=0; j<countOfGroupe; j++){
          
          let number = getRandomInt(3);
          if(!indexGr.includes(number)){
            
            arr.push({user_id: uids[i], group_id: arrWithoutAdmin[number].id})
            indexGr.push(number)
          }
        }
        indexGr = [];
      }
      knex.batchInsert('users_group', arr)
};


