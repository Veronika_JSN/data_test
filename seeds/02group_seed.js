exports.seed = function(knex) {

  return knex('groups').del()
    .then(function () {

      return knex('groups').insert([
        {group_name: 'temporary'},
        {group_name: 'regular'},
        {group_name: 'editors'},
        {group_name: 'admin'}
      ]);
    });
};
