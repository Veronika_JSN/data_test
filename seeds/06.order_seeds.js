exports.seed = async function(knex) {

  await knex('order').del()
    
  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  let uids = (await knex('users')).map(user => user.uid);
  let pids = (await knex('partners')).map(item => item.pid);
  let iids = (await knex('items')).map(item => item.iid);

  
  let arr = [];

  for(let i=1; i<=500000; i++){  
    let part_id = [pids[getRandomInt(pids.length)], null];

    arr.push({
        user_id: uids[getRandomInt(uids.length)], 
        partner_id: part_id[getRandomInt(2)], 
        item_id: iids[getRandomInt(iids.length)]
    })

    if(i % 500 === 0){
      await knex('order').insert(arr)
      arr = [];
    }
  }
};