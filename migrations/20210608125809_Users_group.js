
exports.up = function(knex) {
    return knex.schema.createTable('users_group', table => {
        table.increments('iid');
        table.integer('user_id').unsigned();
        table.integer('group_id').unsigned();
        table.foreign('user_id').references('uid').inTable('users')
        table.foreign('group_id').references('id').inTable('groups')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users_group');
};