exports.up = function(knex) {
    return knex.schema.createTable('users', table => {
        table.increments('uid');
        table.string('login').notNullable();
        table.string('password').notNullable();
        table.date('registration_date');
        table.date('last_visit_date');
        table.string('ip');
        table.boolean('flag');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};
