exports.up = function(knex) {
    return knex.schema.createTable('order', table => {
        table.increments('oid');
        table.integer('user_id').unsigned();
        table.integer('partner_id').unsigned();
        table.integer('item_id').unsigned();
        table.foreign('user_id').references('uid').inTable('users')
        table.foreign('partner_id').references('pid').inTable('partners')
        table.foreign('item_id').references('iid').inTable('items')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('order');
};
