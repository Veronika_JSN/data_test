exports.up = function(knex) {
    return knex.schema.createTable('items', table => {
        table.increments('iid');
        table.string('item_name');
        table.decimal('price').notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('items');
};
