SELECT u.uid, u.login, COUNT(o.oid) as items
FROM default.users as u
JOIN default.order as o ON u.uid = o.user_id
WHERE u.flag = true
AND o.partner_id is null
GROUP BY u.uid;

SELECT u.uid, u.login, sum(i.price) as bill
FROM default.users as u
JOIN default.order as o ON u.uid = o.user_id
JOIN default.items as i ON i.iid = o.item_id
WHERE u.flag = true
AND o.partner_id is null
GROUP BY u.uid;

SELECT p.partner_name, count( DISTINCT o.user_id)
FROM default.partners as p
JOIN default.order as o ON p.pid = o.partner_id
WHERE o.partner_id IS NOT NULL
GROUP BY p.partner_name

SELECT o.partner_id, u.login, sum(i.price)
FROM default.users as u
JOIN default.order as o ON u.uid = o.user_id
JOIN default.items as i ON i.iid = o.item_id
WHERE o.partner_id IS NOT NULL
GROUP BY u.login
ORDER BY o.partner_id

SELECT p.partner_name, sum(i.price)
FROM default.order as o
JOIN default.partners as p ON p.pid = o.partner_id
JOIN default.items as i ON i.iid = o.item_id
GROUP BY p.partner_name
