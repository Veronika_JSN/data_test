module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      port: '3306',
      host: 'localhost',
      database: 'default',
      user:     'default',
      password: 'secret'
    },
    // pool: {
    //   min: 2,
    //   max: 10
    // },
    migrations: {
      directory: './migrations'
    },
    seeds: {
      directory: './seeds'
  }
  },
};
